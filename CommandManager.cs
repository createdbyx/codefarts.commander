﻿/* 
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Commander
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Provides a generic command manager for processing commands.
    /// </summary>
    /// <typeparam name="TKey">The type of key used to specify a command.</typeparam>
    /// <typeparam name="TReturnType">The type of return values that the commands return.</typeparam>
    public class CommandManager<TKey, TReturnType>
    {
        /// <summary>
        /// Used to hold a singleton reference to a <see cref="CommandManager{TKey,TReturnType}"/> type.
        /// </summary>
        private static CommandManager<TKey, TReturnType> singleton;

        /// <summary>
        /// Used to hold references to registered commands.
        /// </summary>
        private readonly IDictionary<TKey, Func<object, TReturnType>> commands = new Dictionary<TKey, Func<object, TReturnType>>();

        /// <summary>
        /// Gets or sets a <see cref="Func{TResult}"/> used to validate weather or not the command is valid.
        /// </summary>
        public Func<TKey, bool> ValidateCommand { get; set; }


        /// <summary>
        /// Returns a <see cref="IEnumerable{T}"/> containing all of the currently registered commands.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<TKey, Func<object, TReturnType>>> Commands()
        {
            foreach (var pair in this.commands)
            {
                yield return pair;
            }
        }

        /// <summary>
        /// Gets the number of commands registered with the <see cref="CommandManager{TKey,TReturnType}"/>.
        /// </summary>
        public int Count
        {
            get
            {
                return this.commands.Count;
            }
        }

        /// <summary>
        /// Gets a singleton instance of a <see cref="CommandManager{TKey,TReturnType}"/> type.
        /// </summary>
        public static CommandManager<TKey, TReturnType> Instance
        {
            get
            {
                if (singleton != null)
                {
                    return singleton;
                }

                return singleton = new CommandManager<TKey, TReturnType>();
            }
        }

        /// <summary>
        /// Gets or sets a default command to use when no matching command can be found.
        /// </summary>
        public TKey DefaultCommand { get; set; }

        /// <summary>
        /// Executes a given command.
        /// </summary>
        /// <typeparam name="T">The type of model that will be passed to the command.</typeparam>
        /// <param name="command">The command to be executed.</param>
        /// <param name="model">A reference to a data model that will be passed to the command.</param>
        /// <returns>Returns the result retrieved from the command.</returns>
        public TReturnType Execute<T>(TKey command, T model)
        {
            try
            {
                // find the command to execute
                command = this.GetCommandToExecute(command);

                // invoke the command
                return this.commands[command].Invoke(model);
            }
            catch
            {
                return default(TReturnType);
            }
        }

        /// <summary>
        /// Registers a command with the <see cref="CommandManager{TKey,TReturnType}"/>.
        /// </summary>
        /// <typeparam name="T">Specifies the type that the command will return.</typeparam>
        /// <param name="command">The command name or identifier.</param>
        /// <param name="callback">A <see cref="Func{TResult}"/> that will be called when the command is executed.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="callback"/> parameter is null.</exception>
        /// <exception cref="ArgumentException">If the command is not valid. Commands are validated with the <see cref="ValidateCommand"/> property.</exception>
        public void Register<T>(TKey command, Func<T, TReturnType> callback)
        {
            // if no callback specified then throw exception
            if (callback == null)
            {
                throw new ArgumentNullException("callback");
            }

            // try to validate the command and throw exception if can not be validated
            if (this.ValidateCommand != null && !this.ValidateCommand(command))
            {
                throw new ArgumentException("Bad command specified!", "command");
            }

            // add the command to the dictionary
            this.commands.Add(command, x => callback((T)x));
        }

        /// <summary>
        /// Used to get a command callback using a command name or identifier.
        /// </summary>
        /// <param name="command">The command to search for.</param>
        /// <returns>Returns a command name or identifier.</returns>
        /// <exception cref="ArgumentException">If unable to find the command or the <see cref="DefaultCommand"/> is also not available.</exception>
        private TKey GetCommandToExecute(TKey command)
        {
            // if a command has been registered then return the same command
            if (this.commands.ContainsKey(command))
            {
                return command;
            }

            // check if the default command property is a string and of so check if it has a value
            // if it is a string and it has no value throw an exception
#if UNITY3D
            if ((this.DefaultCommand is string) && IsNullOrWhiteSpace(this.DefaultCommand as string))
#else
            if ((this.DefaultCommand is string) && string.IsNullOrWhiteSpace(this.DefaultCommand as string))
#endif
            {
                throw new ArgumentException("Command not available!");
            }

            // if the default command name or identifier has been registered as a command then return the default command
            if (this.commands.ContainsKey(this.DefaultCommand))
            {
                return this.DefaultCommand;
            }

            // throw an exception that no commands could be found
            throw new ArgumentException("Command not available!");
        }

#if UNITY3D
        public static bool IsNullOrWhiteSpace(string value)
        {
            if (value == null)
            {
                return true;
            }

            for (var i = 0; i < value.Length; i++)
            {
                if (!char.IsWhiteSpace(value[i]))
                {
                    return false;
                }
            }

            return true;
        }
#endif
    }
}
