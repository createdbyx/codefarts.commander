﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.Commander
{
    using System;

    /// <summary>
    /// Provides extension methods for a <see cref="CommandManager{TKey,TReturnType}"/> type.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Executes a given command.
        /// </summary>
        /// <typeparam name="TKey">The type of key used to specify a command.</typeparam>
        /// <typeparam name="TReturnType">The type of return value that the command returns.</typeparam>
        /// <param name="manager">A reference to a <see cref="CommandManager{TKey,TReturnType}"/> type.</param>
        /// <param name="command">The command to be executed.</param>
        /// <returns>Returns the result retrieved from the command.</returns>
        /// <exception cref="NullReferenceException">If <see cref="manager"/> is null.</exception>
        public static TReturnType Execute<TKey, TReturnType>(this CommandManager<TKey, TReturnType> manager, TKey command)
        {
            return manager.Execute(command, default(TReturnType));
        }

        /// <summary>
        /// Registers a command with the <see cref="CommandManager{TKey,TReturnType}"/>.
        /// </summary>
        /// <typeparam name="TKey">The type of key used to specify a command.</typeparam>
        /// <typeparam name="TReturnType">The type of return value that the command returns.</typeparam>
        /// <param name="manager">A reference to a <see cref="CommandManager{TKey,TReturnType}"/> type.</param>
        /// <param name="command">The command name or identifier.</param>
        /// <param name="callback">A <see cref="Func{TResult}"/> that will be called when the command is executed.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="callback"/> parameter is null.</exception>
        /// <exception cref="ArgumentException">If the command is not valid. Commands are validated with the <see cref="CommandManager{TKey,TReturnType}.ValidateCommand"/> property.</exception>
        public static void Register<TKey, TReturnType>(this CommandManager<TKey, TReturnType> manager, TKey command, Func<TReturnType> callback)
        {
            manager.Register<object>(command, x => callback());
        }

        /// <summary>
        /// Registers a command with the <see cref="CommandManager{TKey,TReturnType}"/>.
        /// </summary>
        /// <typeparam name="T">Specifies the type that the command will accept as a parameter.</typeparam>
        /// <typeparam name="TKey">The type of key used to specify a command.</typeparam>
        /// <typeparam name="TReturnType">The type of return value that the command returns.</typeparam>
        /// <param name="manager">A reference to a <see cref="CommandManager{TKey,TReturnType}"/> type.</param>
        /// <param name="command">The command name or identifier.</param>
        /// <param name="callback">A <see cref="Action{T}"/> that will be called when the command is executed.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="callback"/> parameter is null.</exception>
        /// <exception cref="ArgumentException">If the command is not valid. Commands are validated with the <see cref="CommandManager{TKey,TReturnType}.ValidateCommand"/> property.</exception>
        public static void RegisterAction<T, TKey, TReturnType>(this CommandManager<TKey, TReturnType> manager, TKey command, Action<T> callback)
        {
            manager.Register<T>(command, x =>
                {
                    callback(x);
                    return default(TReturnType);
                });
        }

        /// <summary>
        /// Registers a command with the <see cref="CommandManager{TKey,TReturnType}"/>.
        /// </summary>
        /// <typeparam name="TKey">The type of key used to specify a command.</typeparam>
        /// <typeparam name="TReturnType">The type of return value that the command returns.</typeparam>
        /// <param name="manager">A reference to a <see cref="CommandManager{TKey,TReturnType}"/> type.</param>
        /// <param name="command">The command name or identifier.</param>
        /// <param name="callback">A <see cref="Action"/> that will be called when the command is executed.</param>
        /// <exception cref="ArgumentNullException">If the <see cref="callback"/> parameter is null.</exception>
        /// <exception cref="ArgumentException">If the command is not valid. Commands are validated with the <see cref="CommandManager{TKey,TReturnType}.ValidateCommand"/> property.</exception>
        public static void RegisterAction<TKey, TReturnType>(this CommandManager<TKey, TReturnType> manager, TKey command, Action callback)
        {
            manager.Register<object>(command, x =>
                {
                    callback();
                    return default(TReturnType);
                });
        }
    }
}